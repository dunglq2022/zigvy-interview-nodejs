
const createUserMiddle = (req, res, next) => {
    console.log('create user middile')
    next();
}
const getAllUserMiddle = (req, res, next) => {
    console.log('get all user middile')
    next();   
}
const getUserMiddleById = (req, res, next) => {
    console.log('get user middile')
    next();  
}
const updateUserMiddleById = (req, res, next) => {
    console.log('update user middile')
    next(); 
}
const deleteUserMiddleById = (req, res, next) => {
    console.log('delete user middile')
    next();
}

module.exports = {
    createUserMiddle,
    getAllUserMiddle,
    getUserMiddleById,
    updateUserMiddleById,
    deleteUserMiddleById
}