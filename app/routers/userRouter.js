const express = require('express');
const userRouter = express.Router();
//Import Middile
const {
    createUserMiddle,
    getAllUserMiddle,
    getUserMiddleById,
    updateUserMiddleById,
    deleteUserMiddleById
} = require('../middiles/userMiddile')

//Import Controller
const {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById
} = require('../controllers/userController')

userRouter.post('/users/', createUserMiddle, createUser)
userRouter.get('/users/',getAllUserMiddle, getAllUsers)
userRouter.get('/users/:userId',getUserMiddleById, getUserById)
userRouter.put('/users/:userId',updateUserMiddleById, updateUserById)
userRouter.delete('/users/:userId',deleteUserMiddleById, deleteUserById)

module.exports = {userRouter}