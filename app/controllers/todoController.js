const mongoose = require('mongoose');
const todoModel = require('../models/todoModel')
//Function CRUD
const createTodo = (req, res) => {
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(body.userId)) {
        return res.status(400).json({
            message: 'User Id is valid'
        })
    }
    if(!body.title) {
        return res.status(400).json({
            message: 'Title is required'
        })
    }
    let newTodo = new todoModel({
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title
    })
    todoModel.create(newTodo, (err, todo) => {
        if(err) {
            return res.status(400).json({
                message: 'Error in creating todo'
            })
        } else {
            return res.status(201).json({
                message: 'todo created successfully',
                todo
            })
        }
    })
}
const getAllTodo = (req, res) => {
    let userId = req.query.userId;
    if(userId == null) {
        todoModel.find((err, todo) => {
            if(err) {
                return res.status(500).json({
                    message: 'Error in fetching todo'
                })
            } else {
                return res.status(200).json({
                    message: 'todo fetched successfully',
                    todo
                })
            }
        })
    } else {
        todoModel.find({userId: userId}, (err, todo) => {
            if(err) {
                return res.status(500).json({
                    message: 'Error in fetching todo'
                })
            } else {
                return res.status(200).json({
                    message: 'todo fetched successfully',
                    todo
                })
            }
        })
    }
    
}
const getTodoById = (req, res) => {
    let todoId = req.params.todoId;
    if(!mongoose.Types.ObjectId.isValid(todoId)) {
        return res.status(400).json({
            message: 'todo Id is valid'
        })
    }
    todoModel.findById(todoId, (err, todo) => {
        if(err) {
            return res.status(500).json({
                message: 'Error in fetching todo'
            })
        } else {
            if(todo == null) {
                return res.status(500).json({
                    message: 'todo not found'
                })
            } else {
                return res.status(200).json({
                    message: 'todo fetched successfully',
                    todo
                })
            }
            
        }
    })
}
const updateTodoById = (req, res) => {
    let todoId = req.params.todoId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(todoId)) {
        return res.status(400).json({
            message: 'todo Id is valid'
        })
    }
    if(!body.title) {
        return res.status(400).json({
            message: 'Title is required'
        })
    }
    let updatetodo = {
        userId: body.userId,
        title: body.title
    }
    todoModel.findByIdAndUpdate(todoId, updatetodo, {new: true}, (err, todo) => {
        if(err) {
            return res.status(500).json({
                message: 'Error in updating todo'
            })
        } else {
            return res.status(200).json({
                message: `Update todoId ${todoId}`,
                todo
            })
        }
    })
}
const deleteTodoById = (req, res) => {
    let todoId = req.params.todoId;
    if(!mongoose.Types.ObjectId.isValid(todoId)) {
        return res.status(400).json({
            message: `todo Id is valid`
        })
    }
    todoModel.findByIdAndRemove(todoId, (err, todo) => {
        if(err) {
            return res.status(500).json({
                message: 'Error in deleting todo'
            })
        } else {
            return res.status(204).json({
                message: `todo deleted successfully`
            })
        }
    })
}
const getTodosOfUser = (req, res) => {
    let userId = req.params.userId;
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: `User Id is valid`
        })
    }
    todoModel.find({userId: userId}, (err, todos) => {
        if(err) {
            return res.status(500).json({
                message: 'Error in getting todos'
            })
        } else {
            return res.status(200).json({
                message: `todos of user ${userId}`,
                todos
            })
        }
    })
}
module.exports = {
    createTodo,
    getAllTodo,
    getTodoById,
    updateTodoById,
    deleteTodoById,
    getTodosOfUser
}