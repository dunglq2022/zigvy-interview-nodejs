const express = require('express');
const commmentRouter = express.Router();
//Import middile
const {
    createCommentMiddle,
    getAllCommentMiddle,
    getCommentMiddleById,
    updateCommentMiddleById,
    deleteCommentMiddleById
} = require ('../middiles/commentMiddle')
//Import controller
const {
    createComment,
    getAllComment,
    getCommentById,
    updateCommentById,
    deleteCommentById
} = require('../controllers/commentController')

commmentRouter.post('/comments', createCommentMiddle, createComment)
commmentRouter.get('/comments', getAllCommentMiddle, getAllComment)
commmentRouter.get('/comments/:commentId', getCommentMiddleById, getCommentById)
commmentRouter.put('/comments/:commentId', updateCommentMiddleById, updateCommentById)
commmentRouter.delete('/comments/:commentId', deleteCommentMiddleById, deleteCommentById)

module.exports = {commmentRouter}