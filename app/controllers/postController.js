const mongoose = require('mongoose')
const postModel = require('../models/postModel')
//Function CRUD
const createPost = (req, res) => {
    let body = req.body;
    if(!body.title) {
        return res.status(400).json({
            message:`Title is require`
        })
    }
    if(!body.body) {
        return res.status(400).json({
            message:`Body is require`
        })
    }
    if(!mongoose.Types.ObjectId.isValid(body.userId)) {
        return res.status(400).json({
            message:`User Id is valid`
        })
    }
    let newPost = new postModel({
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title,
        body: body.body,
    })
    postModel.create(newPost, (err, post) => {
        if(err) {
            return res.status(400).json({
                message: `Err not create post`
            })
        } else {
            return res.status(201).json({
                message: `Create post success`,
                post: post
            })
        }
    })
}

const getAllPost = (req, res) => {
    postModel.find({}, (err, posts) => {
        if(err) {
            return res.status(400).json({
                message: `Err not get all post`
            })
        } else {
            return res.status(200).json({
                message: `Get all post success`,
                posts: posts
            })
        }
    })
}

const getPostById = (req, res) => {
    let postId = req.params.postId;
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            message: `postId is valid`
        })
    }
    postModel.findById(postId, (err, post) => {
        if(err) {
            return res.status(400).json({
                message: `Err not find post`
            })
        } else {
            if(post == null) {
                return res.status(400).json({
                    message: `Post not found`
                })
            } else {
                return res.status(200).json({
                    message: `Find post success`,
                    post: post
                })
            }
            
        }
    })
}

const updatePostById = (req, res) => {
    let postId = req.params.postId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(postId))
    if(!body.title) {
        return res.status(400).json({
            message:`Title is require`
        })
    }
    if(!body.body) {
        return res.status(400).json({
            message:`Body is require`
        })
    }
    if(!mongoose.Types.ObjectId.isValid(body.userId)) {
        return res.status(400).json({
            message:`User Id is valid`
        })
    }
    let updatePost = {
        title: body.title,
        body: body.body,
    }
    postModel.findByIdAndUpdate(postId, updatePost, {new: true}, (err, post) => {
        if(err) {
            return res.status(500).json({
                message: `Err update post`
            })
        } else {
            return res.status(200).json({
                message: `Update post success`,
                post: post
            })
        }
    })
}

const deletePostById = (req, res) => {
    let postId = req.params.postId;
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            message: `Post Id is valid`
        })
    }
    postModel.findByIdAndDelete(postId, (err, post) => {
        if(err) {
            return res.status(500).json({
                message: `Err delete post`
            })
        } else {
            return res.status(204).json({
                message: `Delete post success`
            })
        }
    })
}

const getPostsOfUser = (req, res) => {
    let userId = req.params.userId;
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: `User Id is valid`
        })
    }
    postModel.find({userId: userId}).exec((err, posts) => {
        if(err) {
            return res.status(500).json({
                message: `Err 500 not found userId ${userId}`
            })
        } else {
            return res.status(200).json({
                message: `Get posts of user success`,
                posts: posts
            })
        }
    })
}

const getAllPostOfUser = (req, res) => {
    let userId = req.query.userId;
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: `User Id is valid`
        })
    }
    postModel.find({userId: userId}).exec((err, posts) => {
        if(err) {
            return res.status(500).json({
                message: `Err 500 not found userId ${userId}`
            })
        } else {
            return res.status(200).json({
                message: `Get posts of user success ${userId}`,
                posts: posts
            })
        }
    })
}
module.exports = {
    createPost,
    getAllPost,
    getPostById,
    updatePostById,
    deletePostById,
    getPostsOfUser,
    getAllPostOfUser
}