const mongoose = require('mongoose');
const albumModel = require('../models/albumsModel')
//Function CRUD
const createAlbum = (req, res) => {
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(body.userId)) {
        return res.status(400).json({
            message: 'User Id is valid'
        })
    }
    if(!body.title) {
        return res.status(400).json({
            message: 'Title is required'
        })
    }
    let newAlbum = new albumModel({
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title
    })
    albumModel.create(newAlbum, (err, album) => {
        if(err) {
            return res.status(400).json({
                message: 'Error in creating album'
            })
        } else {
            return res.status(201).json({
                message: 'Album created successfully',
                album
            })
        }
    })
}
const getAllAlbum = (req, res) => {
    let userId = req.query.userId;
    if(userId == null) {
        albumModel.find((err, album) => {
            if(err) {
                return res.status(500).json({
                    message: 'Error in fetching album'
                })
            } else {
                return res.status(200).json({
                    message: 'Album fetched successfully',
                    album
                })
            }
        })
    } else {
        albumModel.find({userId: userId}, (err, album) => {
            if(err) {
                return res.status(500).json({
                    message: 'Error in fetching album'
                })
            } else {
                return res.status(200).json({
                    message: 'Album fetched successfully',
                    album
                })
            }
        })
    }
    
}
const getAlbumById = (req, res) => {
    let albumId = req.params.albumId;
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            message: 'Album Id is valid'
        })
    }
    albumModel.findById(albumId, (err, album) => {
        if(err) {
            return res.status(500).json({
                message: 'Error in fetching album'
            })
        } else {
            if(album == null) {
                return res.status(500).json({
                    message: 'Album not found'
                })
            } else {
                return res.status(200).json({
                    message: 'Album fetched successfully',
                    album
                })
            }
            
        }
    })
}
const updateAlbumById = (req, res) => {
    let albumId = req.params.albumId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            message: 'Album Id is valid'
        })
    }
    if(!body.title) {
        return res.status(400).json({
            message: 'Title is required'
        })
    }
    let updateAlbum = new albumModel({
        userId: body.userId,
        title: body.title
    })
    albumModel.findByIdAndUpdate(albumId, updateAlbum, {new: true}, (err, album) => {
        if(err) {
            return res.status(500).json({
                message: 'Error in updating album'
            })
        } else {
            return res.status(200).json({
                message: `Update albumId ${albumId}`,
                album
            })
        }
    })
}
const deleteAlbumById = (req, res) => {
    let albumId = req.params.albumId;
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            message: `Album Id is valid`
        })
    }
    albumModel.findByIdAndRemove(albumId, (err, album) => {
        if(err) {
            return res.status(500).json({
                message: 'Error in deleting album'
            })
        } else {
            return res.status(204).json({
                message: `Album deleted successfully`
            })
        }
    })
}
const getAlbumsOfUser = (req, res) => {
    let userId = req.params.userId;
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: `User Id is valid`
        })
    }
    albumModel.find({userId: userId}, (err, albums) => {
        if(err) {
            return res.status(500).json({
                message: 'Error in getting albums'
            })
        } else {
            return res.status(200).json({
                message: `Albums of user ${userId}`,
                albums
            })
        }
    })
}
module.exports = {
    createAlbum,
    getAllAlbum,
    getAlbumById,
    updateAlbumById,
    deleteAlbumById,
    getAlbumsOfUser
}