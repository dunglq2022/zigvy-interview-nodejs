const mongoose = require('mongoose');
const userModel =  require('../models/userModel');
//Function CRUD
const createUser = (req, res) => {
    let body = req.body;
    console.log(body)
    if(!body.name) {
        return res.status(400).json({
            message:`Name is require`
        })
    }
    if(!body.name) {
        return res.status(400).json({
            message:`Name is require`
        })
    }
    if(!body.email) {
        return res.status(400).json({
            message:`Email is require`
        })
    }
    if(!body.address) {
        return res.status(400).json({
            message:`Address is require`
        })
    }
    if(!body.phone) {
        return res.status(400).json({
            message:`Phone is require`
        })
    }
    if(!body.website) {
        return res.status(400).json({
            message:`Website is require`
        })
    }
    if(!body.company) {
        return res.status(400).json({
            message:`Company is require`
        })
    }
    let newUser = new userModel({
    _id: mongoose.Types.ObjectId(),
    name: body.name,
    username: body.username,
    email: body.email,
    address: body.address,
    phone: body.phone,
    website: body.website,
    company: body.company
})
    userModel.create(newUser, (err, user) => {
        if(err) {
            return res.status(500).json({
                message:`Err 500 not create user`
            })
        } else {
            return res.status(201).json({
                message:`User created`,
                user: user
            })
        }
    })
}

const getAllUsers = (req, res) => {
    userModel.find((err, users) =>{
        if(err) {
            return res.status(500).json({
                message:`Err 500 not get users`
            })
        } else {
            return res.status(200).json({
                message:`Users get`,
                users: users
            })
        }
    })
}

const getUserById = (req, res) => {
    let userId = req.params.userId;
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message:`User id is not valid`
        })
    }
    userModel.findById(userId, (err, user) => {
        if(err) {
            return res.status(500).json({
                message:`Err 500 not get user`
            })
        } else {
            return res.status(200).json({
                message:`User get`,
                user: user
            })
        }
    })
}

const updateUserById = (req, res) => {
    let userId = req.params.userId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(userId))
    if(!body.name) {
        return res.status(400).json({
            message:`Name is require`
        })
    }
    if(!body.name) {
        return res.status(400).json({
            message:`Name is require`
        })
    }
    if(!body.email) {
        return res.status(400).json({
            message:`Email is require`
        })
    }
    if(!body.address) {
        return res.status(400).json({
            message:`Address is require`
        })
    }
    if(!body.phone) {
        return res.status(400).json({
            message:`Phone is require`
        })
    }
    if(!body.website) {
        return res.status(400).json({
            message:`Website is require`
        })
    }
    if(!body.company) {
        return res.status(400).json({
            message:`Company is require`
        })
    }
    let updateUser = {
    name: body.name,
    username: body.username,
    email: body.email,
    address: body.address,
    phone: body.phone,
    website: body.website,
    company: body.company
    }
    userModel.findByIdAndUpdate(userId, updateUser, (err, user) => {
        if(err) {
            return res.status(400).json({
                message: `Error updating user`
            })
        } else {
            return res.status(200).json({
                message: `Update user success`,
                user: user
            })
        }
    })
}

const deleteUserById = (req, res) => {
    const userId = req.params.userId
    userModel.findByIdAndRemove(userId, (err, user) => {
        if(err) {
            return res.status(500).json({
                message: `Error deleting user`
            })
        } else {
            return res.status(204).json({
                message: `Delete user success`
            })
        }
    })
}
module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById
}