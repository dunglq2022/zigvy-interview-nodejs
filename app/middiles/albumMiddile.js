
const createAlbumMiddle = (req, res, next) => {
    console.log('create Album middile')
    next();
}
const getAllAlbumMiddle = (req, res, next) => {
    console.log('get all Album middile')
    next();   
}
const getAlbumMiddleById = (req, res, next) => {
    console.log('get Album middile')
    next();  
}
const updateAlbumMiddleById = (req, res, next) => {
    console.log('update Album middile')
    next(); 
}
const deleteAlbumMiddleById = (req, res, next) => {
    console.log('delete Album middile')
    next();
}

module.exports = {
    createAlbumMiddle,
    getAllAlbumMiddle,
    getAlbumMiddleById,
    updateAlbumMiddleById,
    deleteAlbumMiddleById
}