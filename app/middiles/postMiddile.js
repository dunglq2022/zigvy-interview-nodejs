
const createPostMiddle = (req, res, next) => {
    console.log('create Post middile')
    next();
}
const getAllPostMiddle = (req, res, next) => {
    console.log('get all Post middile')
    next();   
}
const getPostMiddleById = (req, res, next) => {
    console.log('get Post middile')
    next();  
}
const updatePostMiddleById = (req, res, next) => {
    console.log('update Post middile')
    next(); 
}
const deletePostMiddleById = (req, res, next) => {
    console.log('delete Post middile')
    next();
}

module.exports = {
    createPostMiddle,
    getAllPostMiddle,
    getPostMiddleById,
    updatePostMiddleById,
    deletePostMiddleById
}