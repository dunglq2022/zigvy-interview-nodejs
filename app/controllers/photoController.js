const mongoose = require('mongoose');
const photoModel = require('../models/photoModel')
//Function CRUD
const createPhoto = (req, res) => {
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(body.albumId)) {
        return res.status(400).json({
            message: 'Albums Id is valid'
        })
    }
    if(!body.title) {
        return res.status(400).json({
            message: 'Title is required'
        })
    }
    if(!body.url) {
        return res.status(400).json({
            message: 'Url is required'
        })
    }
    if(!body.thumbnailUrl) {
        return res.status(400).json({
            message: 'Thumbnail Url is required'
        })
    }

    let newPhoto = new photoModel({
        _id: mongoose.Types.ObjectId(),
        albumId: body.albumId,
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl
    })
    photoModel.create(newPhoto, (err, photo) => {
        if(err) {
            return res.status(400).json({
                message: 'Error in creating photo'
            })
        } else {
            return res.status(201).json({
                message: 'Photo created successfully',
                photo
            })
        }
    })
}
const getAllPhoto = (req, res) => {
    let albumId = req.query.albumId;
    if(albumId == null) {
        photoModel.find((err, photo) => {
            if(err) {
                return res.status(500).json({
                    message: 'Error in fetching photo'
                })
            } else {
                return res.status(200).json({
                    message: 'Photo fetched successfully',
                    photo
                })
            }
        })
    } else {
        photoModel.find({albumId: albumId}, (err, photo) => {
            if(err) {
                return res.status(500).json({
                    message: 'Error in fetching photo'
                })
            } else {
                return res.status(200).json({
                    message: 'Photo fetched successfully',
                    photo
                })
            }
        })
    }
    
}
const getPhotoById = (req, res) => {
    let photoId = req.params.photoId;
    if(!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            message: 'Photo Id is valid'
        })
    }
    photoModel.findById(photoId, (err, photo) => {
        if(err) {
            return res.status(500).json({
                message: 'Error in fetching photo'
            })
        } else {
            if(photo == null) {
                return res.status(500).json({
                    message: 'Photo not found'
                })
            } else {
                return res.status(200).json({
                    message: 'Photo fetched successfully',
                    photo
                })
            }
            
        }
    })
}
const updatePhotoById = (req, res) => {
    let photoId = req.params.photoId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(body.albumId)) {
        return res.status(400).json({
            message: 'Albums Id is valid'
        })
    }
    if(!body.title) {
        return res.status(400).json({
            message: 'Title is required'
        })
    }
    if(!body.url) {
        return res.status(400).json({
            message: 'Url is required'
        })
    }
    if(!body.thumbnailUrl) {
        return res.status(400).json({
            message: 'Thumbnail Url is required'
        })
    }

    let updatePhoto = {
        albumId: body.albumId,
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl
    }
    photoModel.findByIdAndUpdate(photoId, updatePhoto, {new: true}, (err, photo) => {
        if(err) {
            return res.status(500).json({
                message: 'Error in updating photo'
            })
        } else {
            return res.status(200).json({
                message: `Update photoId ${photoId}`,
                photo
            })
        }
    })
}
const deletePhotoById = (req, res) => {
    let photoId = req.params.photoId;
    if(!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            message: `Photo Id is valid`
        })
    }
    photoModel.findByIdAndRemove(photoId, (err, photo) => {
        if(err) {
            return res.status(500).json({
                message: 'Error in deleting photo'
            })
        } else {
            return res.status(204).json({
                message: `Photo deleted successfully`
            })
        }
    })
}
const getPhotosOfAlbum = (req, res) => {
    let albumId = req.params.albumId;
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            message: `Album Id is valid`
        })
    }
    photoModel.find({albumId: albumId}, (err, photos) => {
        if(err) {
            return res.status(500).json({
                message: 'Error in getting photos'
            })
        } else {
            return res.status(200).json({
                message: `photos of album ${albumId}`,
                photos
            })
        }
    })
}
module.exports = {
    createPhoto,
    getAllPhoto,
    getPhotoById,
    updatePhotoById,
    deletePhotoById,
    getPhotosOfAlbum
}