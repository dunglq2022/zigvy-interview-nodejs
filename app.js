const express = require('express');
const mongoose = require('mongoose');
const port = 8000;
const app = express();
const {userRouter} = require('./app/routers/userRouter');
const { postRouter } = require('./app/routers/postRouter');
const { commmentRouter } = require('./app/routers/commentRouter');
const { albumRouter } = require('./app/routers/albumRouter');
const { photoRouter } = require('./app/routers/photoRouter');
const { todoRouter } = require('./app/routers/todoRouter');
app.use(express.json());

//Connect MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Zigvy", (err) => {
    if(err) throw err;

    console.log("Connect MongoDB Successfully!")
})

app.use('/', userRouter)
app.use('/', postRouter)
app.use('/', commmentRouter)
app.use('/', albumRouter)
app.use('/', photoRouter)
app.use('/', todoRouter)

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
})