const express = require('express');
const postRouter = express.Router();
//Import middile
const {
    createPostMiddle,
    getAllPostMiddle,
    getPostMiddleById,
    updatePostMiddleById,
    deletePostMiddleById
} = require('../middiles/postMiddile')

//Import controller
const {
    createPost,
    getAllPost,
    getPostById,
    updatePostById,
    deletePostById,
    getPostsOfUser,
    getAllPostOfUser
} = require('../controllers/postController')

postRouter.post('/posts/', createPostMiddle, createPost)
postRouter.get('/posts/', getAllPostMiddle, getAllPost)
postRouter.get('/posts/:postId', getPostMiddleById, getPostById)
postRouter.put('/posts/:postId', updatePostMiddleById, updatePostById)
postRouter.delete('/posts/:postId', deletePostMiddleById, deletePostById)
postRouter.get('/users/:userId/posts',getAllPostMiddle , getPostsOfUser)
postRouter.get('/post/',getAllPostMiddle, getAllPostOfUser)

module.exports = {postRouter}