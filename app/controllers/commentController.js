const mongoose = require('mongoose');
const commentModel = require('../models/commentModel')
//Function CRUD
const createComment = (req, res) => {
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(body.postId)) {
        return res.status(400).json({
            message: `postId is valid`
        })
    }
    if(!body.name) {
        return res.status(400).json({
            message: `name is required`
        })
    }
    if(!body.email) {
        return res.status(400).json({
            message: `email is required`
        })
    }
    if(!body.body) {
        return res.status(400).json({
            message: `body is required`
        })
    }
    let newComment = new commentModel({
        _id: mongoose.Types.ObjectId(),
        postId: body.postId,
        name: body.name,
        email: body.email,
        body: body.body
    })
    commentModel.create(newComment, (err, commnet) => {
        if(err) {
            return res.status(500).json({
                message: `Error in creating comment`,
            })
        } else {
            return res.status(201).json({
                message: `Comment created successfully`,
                comments: commnet
            })
        }
    })
}
const getAllComment = (req, res) => {
    commentModel.find({}, (err, comments) => {
        if(err) {
            return res.status(500).json({
                message: `Error in getting comments`,
            })
        } else {
            return res.status(200).json({
                message: `Comments fetched successfully`,
                comments: comments
            })
        }
    })
}
const getCommentById = (req, res) => {
    let commentId = req.params.commentId
    if(!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            message: `Invalid comment id`
        })
    }
    commentModel.findById(commentId, (err, comment) => {
        if(err) {
            return res.status(500).json({
                message: `Error in get comment`
            })
        } else {
            if(comment == null) {
                return res.status(500).json({
                    message: `Comment not found`
                })
            } else {
                return res.status(200).json({
                    message: `Comment found successfully`,
                    comment: comment
                })
            }
            
        }
    })
}
const updateCommentById = (req, res) => {
    let commentId = req.params.commentId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(body.postId)) {
        return res.status(400).json({
            message: `postId is valid`
        })
    }
    if(!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            message: `postId is valid`
        })
    }
    if(!body.name) {
        return res.status(400).json({
            message: `name is required`
        })
    }
    if(!body.email) {
        return res.status(400).json({
            message: `email is required`
        })
    }
    if(!body.body) {
        return res.status(400).json({
            message: `body is required`
        })
    }
    let updateComment = {
        postId: body.postId,
        name: body.name,
        email: body.email,
        body: body.body
    }
    commentModel.findByIdAndUpdate(commentId, updateComment, (err, comment) => {
        if(err) {
            return res.status(500).json({
                message: `Internal server error`
            })
        } else {
            return res.status(200).json({
                message: `Comment updated successfully`,
                comment: comment
            })
        }
    })
}
const deleteCommentById = (req, res) => {
    let commentId = req.params.commentId;
    if(!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            message: `comment Id is valid`
        })
    }
    commentModel.findByIdAndRemove(commentId, (err, comment) => {
        if(err) {
            return res.status(500).json({
                message: `Internal server error`
            })
        } else {
            return res.status(204).json({
                message: `Delete comment `
            })
        }
    })
}
module.exports = {
    createComment,
    getAllComment,
    getCommentById,
    updateCommentById,
    deleteCommentById
}