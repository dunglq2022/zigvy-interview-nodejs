
const createCommentMiddle = (req, res, next) => {
    console.log('create Comment middile')
    next();
}
const getAllCommentMiddle = (req, res, next) => {
    console.log('get all Comment middile')
    next();   
}
const getCommentMiddleById = (req, res, next) => {
    console.log('get Comment middile')
    next();  
}
const updateCommentMiddleById = (req, res, next) => {
    console.log('update Comment middile')
    next(); 
}
const deleteCommentMiddleById = (req, res, next) => {
    console.log('delete Comment middile')
    next();
}

module.exports = {
    createCommentMiddle,
    getAllCommentMiddle,
    getCommentMiddleById,
    updateCommentMiddleById,
    deleteCommentMiddleById
}