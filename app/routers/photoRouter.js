const express = require('express');
const photoRouter = express.Router();
//Import middile
const {
    createPhotoMiddle,
    getAllPhotoMiddle,
    getPhotoMiddleById,
    updatePhotoMiddleById,
    deletePhotoMiddleById
} = require('../middiles/photoMiddile')
//Import controller
const {
    createPhoto,
    getAllPhoto,
    getPhotoById,
    updatePhotoById,
    deletePhotoById,
    getPhotosOfAlbum
} = require('../controllers/photoController')

photoRouter.post('/photos', createPhotoMiddle, createPhoto)
photoRouter.get('/photos', getAllPhotoMiddle, getAllPhoto)
photoRouter.get('/photos/:photoId',getPhotoMiddleById, getPhotoById)
photoRouter.put('/photos/:photoId', updatePhotoMiddleById, updatePhotoById)
photoRouter.delete('/photos/:photoId', deletePhotoMiddleById, deletePhotoById)
photoRouter.get('/albums/:albumId/photos', getAllPhotoMiddle, getPhotosOfAlbum)

module.exports = {photoRouter}