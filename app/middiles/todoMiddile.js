
const createTodoMiddle = (req, res, next) => {
    console.log('create Todo middile')
    next();
}
const getAllTodoMiddle = (req, res, next) => {
    console.log('get all Todo middile')
    next();   
}
const getTodoMiddleById = (req, res, next) => {
    console.log('get Todo middile')
    next();  
}
const updateTodoMiddleById = (req, res, next) => {
    console.log('update Todo middile')
    next(); 
}
const deleteTodoMiddleById = (req, res, next) => {
    console.log('delete Todo middile')
    next();
}

module.exports = {
    createTodoMiddle,
    getAllTodoMiddle,
    getTodoMiddleById,
    updateTodoMiddleById,
    deleteTodoMiddleById
}