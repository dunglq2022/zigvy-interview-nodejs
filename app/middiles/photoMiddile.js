
const createPhotoMiddle = (req, res, next) => {
    console.log('create Photo middile')
    next();
}
const getAllPhotoMiddle = (req, res, next) => {
    console.log('get all Photo middile')
    next();   
}
const getPhotoMiddleById = (req, res, next) => {
    console.log('get Photo middile')
    next();  
}
const updatePhotoMiddleById = (req, res, next) => {
    console.log('update Photo middile')
    next(); 
}
const deletePhotoMiddleById = (req, res, next) => {
    console.log('delete Photo middile')
    next();
}

module.exports = {
    createPhotoMiddle,
    getAllPhotoMiddle,
    getPhotoMiddleById,
    updatePhotoMiddleById,
    deletePhotoMiddleById
}