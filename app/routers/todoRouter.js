const express = require('express');
const todoRouter = express.Router();
//Import middile
const {
    createTodoMiddle,
    getAllTodoMiddle,
    getTodoMiddleById,
    updateTodoMiddleById,
    deleteTodoMiddleById
} = require('../middiles/todoMiddile')
//Import controller
const {
    createTodo,
    getAllTodo,
    getTodoById,
    updateTodoById,
    deleteTodoById,
    getTodosOfUser
} = require('../controllers/todoController')

todoRouter.post('/todos', createTodoMiddle, createTodo)
todoRouter.get('/todos', getAllTodoMiddle, getAllTodo)
todoRouter.get('/todos/:todoId',getTodoMiddleById, getTodoById)
todoRouter.put('/todos/:todoId', updateTodoMiddleById, updateTodoById)
todoRouter.delete('/todos/:todoId', deleteTodoMiddleById, deleteTodoById)
todoRouter.get('/users/:userId/todos', getAllTodoMiddle, getTodosOfUser)

module.exports = {todoRouter}