const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const todoSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    userId: {
        type: mongoose.Types.ObjectId,
        ref: 'user'
    },
    title: {
        type: String,
        require: true
    },
    completed: {
        type: Boolean,
        default: false,
    }
})

module.exports = mongoose.model('todo', todoSchema)